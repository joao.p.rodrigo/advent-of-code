package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	f, err := os.Open("source.txt")
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(f)

	v := 0
	for scanner.Scan() {
		v += extractWrittenNumbers(scanner.Text())
	}

	fmt.Println(v)
}

var writtenNumbers = []string{
	"one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
}

type digitWithPosition struct {
	digit    int
	position int
	found    bool
}

func extractWrittenNumbers(s string) int {
	digits := make([]digitWithPosition, 0, 2)

	for i, r := range s {
		if r >= '0' && r <= '9' {
			digits = append(digits, digitWithPosition{digit: int(r - '0'), position: i})
		}
	}

	var firstWord, lastWord digitWithPosition

	for i, word := range writtenNumbers {
		f := strings.Index(s, word)
		if f >= 0 {
			if !firstWord.found || f < firstWord.position {
				firstWord = digitWithPosition{
					found:    true,
					digit:    i + 1,
					position: f,
				}
			}
		}

		l := strings.LastIndex(s, word)
		if l >= 0 {
			if !lastWord.found || l > lastWord.position {
				lastWord = digitWithPosition{
					found:    true,
					digit:    i + 1,
					position: l,
				}
			}
		}
	}

	n0 := digits[0]
	n1 := digits[len(digits)-1]

	if firstWord.found {
		if firstWord.position < n0.position {
			n0.digit = firstWord.digit
		}
	}

	if lastWord.found {
		if lastWord.position > n1.position {
			n1.digit = lastWord.digit
		}
	}

	return n0.digit*10 + n1.digit
}
