package main

import (
	"fmt"
	"strings"
)

type Direction int

const (
	DirUp Direction = 1 << iota
	DirDown
	DirLeft
	DirRight
	DirImpossible
	DirAny
)

func (d Direction) String() string {
	dirs := []string{}

	if d&DirUp == DirUp {
		dirs = append(dirs, "DirUp")
	}
	if d&DirDown == DirDown {
		dirs = append(dirs, "DirDown")
	}
	if d&DirLeft == DirLeft {
		dirs = append(dirs, "DirLeft")
	}
	if d&DirRight == DirRight {
		dirs = append(dirs, "DirRight")
	}

	return strings.Join(dirs, "|")
}

func (d Direction) Apply(x, y int, prev Direction) (int, int, Direction) {
	next := d &^ prev

	switch next {
	case DirUp:
		return x, y - 1, DirDown

	case DirDown:
		return x, y + 1, DirUp

	case DirLeft:
		return x - 1, y, DirRight

	case DirRight:
		return x + 1, y, DirLeft

	default:
		panic(fmt.Sprintf("cant compute direction %s (direction %s prev %s)", next, d, prev))
	}
}
