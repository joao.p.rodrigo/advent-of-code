package main

import (
	"bytes"
)

func (m PipeMap) CleanCanvas() PipeMap {
	canvas := make([][]byte, 0, len(m.pipes))

	for range m.pipes {
		row := make([]byte, len(m.pipes[0]))

		for ii := range row {
			row[ii] = '.'
		}

		canvas = append(canvas, row)
	}

	startDirection := m.startDirections()[0]

	x0 := startDirection.nextX
	y0 := startDirection.nextY
	prev0 := startDirection.prevDirection
	canvas[y0][x0] = m.pipes[y0][x0]

	for {
		x0, y0, prev0 = m.next(x0, y0, prev0)
		canvas[y0][x0] = m.pipes[y0][x0]

		if x0 == m.startX && y0 == m.startY {
			return PipeMap{
				pipes:  canvas,
				startX: m.startX,
				startY: m.startY,
			}
		}
	}
}

type Point struct{ x, y int }

func (m PipeMap) isEmpty(x, y int) bool {
	if x < 0 || y < 0 || x >= len(m.pipes[0]) || y >= len(m.pipes) {
		return false
	}

	switch m.pipes[y][x] {
	case byte(0), '.':
		return true
	default:
		return false
	}
}

func (m PipeMap) CountPainted() int {
	count := 0

	for _, b := range bytes.Join(m.pipes, []byte("\n")) {
		if b == '.' {
			count++
		}
	}

	return count
}

func (m *PipeMap) Expand() {
	canvas := make([][]byte, 0, len(m.pipes)*3)

	for _, row := range m.pipes {
		newRow := make([]byte, len(m.pipes[0])*3)

		canvas = append(canvas, make([]byte, len(m.pipes[0])*3))
		for i := 0; i < len(row); i++ {
			newRow[i*3+1] = row[i]
		}
		canvas = append(canvas, newRow)
		canvas = append(canvas, make([]byte, len(m.pipes[0])*3))
	}

	for y := 1; y < len(canvas); y += 3 {
		for x := 1; x < len(canvas[0]); x += 3 {
			switch canvas[y][x] {
			case '-':
				canvas[y][x-1] = '-'
				canvas[y][x+1] = '-'
			case '|':
				canvas[y-1][x] = '|'
				canvas[y+1][x] = '|'
			case '7':
				canvas[y+1][x] = '|'
				canvas[y][x-1] = '-'
			case 'F':
				canvas[y][x+1] = '-'
				canvas[y+1][x] = '|'
			case 'J':
				canvas[y-1][x] = '|'
				canvas[y][x-1] = '-'

			case 'L':
				canvas[y-1][x] = '|'
				canvas[y][x+1] = '-'
			case 'S':
				canvas[y-1][x] = 'S'
				canvas[y+1][x] = 'S'
				canvas[y][x+1] = 'S'
				canvas[y][x-1] = 'S'
			}
		}
	}

	m.pipes = canvas
}

func (m PipeMap) Paint() {
	var queue []Point

	var doDrop func(int, int)
	doDrop = func(x, y int) {
		if !m.isEmpty(x, y) {
			return
		}

		m.pipes[y][x] = 'O'

		queue = append(queue,
			Point{x - 1, y},
			Point{x + 1, y},
			Point{x, y - 1},
			Point{x, y + 1},
		)
	}

	queue = append(queue, Point{0, 0})
	i := 0
	for len(queue)-i > 0 {
		next := queue[i]
		i++
		doDrop(next.x, next.y)
	}
}
