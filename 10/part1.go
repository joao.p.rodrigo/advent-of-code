package main

func (m PipeMap) traverseLength() int {
	startDirections := m.startDirections()

	count := 1
	var x0, y0, x1, y1 int
	var prev0, prev1 Direction

	x0 = startDirections[0].nextX
	y0 = startDirections[0].nextY
	prev0 = startDirections[0].prevDirection

	x1 = startDirections[1].nextX
	y1 = startDirections[1].nextY
	prev1 = startDirections[1].prevDirection

	for {
		count++
		x0, y0, prev0 = m.next(x0, y0, prev0)

		if x0 == x1 && y0 == y1 {
			return count
		}

		x1, y1, prev1 = m.next(x1, y1, prev1)
		if x0 == x1 && y0 == y1 {
			return count
		}

	}
}
