package main

import (
	"bytes"
	"fmt"
	"os"
)

type Pipe byte

func main() {
	data, err := os.ReadFile("source.txt")
	if err != nil {
		panic(err)
	}

	tiles := bytes.Split(data, []byte("\n"))

	pm := PipeMap{pipes: tiles}
	pm.FindStartPosition()

	// count := pm.traverseLength()
	// fmt.Println(count)

	canvas := pm.CleanCanvas()
	// canvas.PaintSides()

	canvas.Expand()
	canvas.Paint()

	fmt.Println(canvas.CountPainted())
}

func (m *PipeMap) FindStartPosition() {
	for y, row := range m.pipes {
		for x, cel := range row {
			if cel == 'S' {
				m.startX = x
				m.startY = y
				return
			}
		}
	}

	panic("start not found")
}

type PipeMap struct {
	pipes  [][]byte
	startX int
	startY int
}

func (p Pipe) directions() Direction {
	switch p {
	case '|':
		return DirUp | DirDown
	case '-':
		return DirLeft | DirRight
	case 'L':
		return DirUp | DirRight
	case 'J':
		return DirUp | DirLeft
	case '7':
		return DirLeft | DirDown
	case 'F':
		return DirRight | DirDown
	case '.':
		return DirImpossible
	case 'S':
		return DirAny
	default:
		panic(fmt.Sprintf("unknown direction %c", p))
	}
}

func (m PipeMap) next(x, y int, prev Direction) (int, int, Direction) {
	return m.pipeCoords(x, y).directions().Apply(x, y, prev)
}

func (m PipeMap) pipeCoords(x, y int) Pipe {
	return Pipe(m.pipes[y][x])
}

type startCondition struct {
	nextX         int
	nextY         int
	prevDirection Direction
}

func (s startCondition) Valid(m PipeMap) bool {
	if s.nextX < 0 && s.nextY < 0 {
		return false
	}

	if m.pipeCoords(s.nextX, s.nextY).directions()&s.prevDirection == s.prevDirection {
		return true
	}

	return false
}

func (m PipeMap) startDirections() []startCondition {
	directions := []startCondition{}
	x := m.startX
	y := m.startY

	s := startCondition{x - 1, y, DirRight}
	if s.Valid(m) {
		directions = append(directions, s)
	}

	s = startCondition{x + 1, y, DirLeft}
	if s.Valid(m) {
		directions = append(directions, s)
	}

	s = startCondition{x, y - 1, DirDown}
	if s.Valid(m) {
		directions = append(directions, s)
	}

	s = startCondition{x, y + 1, DirUp}
	if s.Valid(m) {
		directions = append(directions, s)
	}

	return directions
}
