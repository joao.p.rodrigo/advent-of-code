package main

import (
	"bytes"
	"fmt"
	"os"
	"strings"
)

var testData = `
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
..........
111.111...
...*......
`

func main() {
	data := []byte(strings.TrimSpace(testData))

	matrix := bytes.Split(bytes.TrimSpace(data), []byte("\n"))

	if gearSum(matrix) != 467835+12321 {
		fmt.Println("Failed")
		return
	} else {
		fmt.Println("Succeeded")
	}

	data, err := os.ReadFile("source.txt")
	if err != nil {
		panic(err)
	}
	matrix = bytes.Split(bytes.TrimSpace(data), []byte("\n"))

	fmt.Println(gearSum(matrix))
}

func gearSum(matrix [][]byte) int {
	gearSum := 0

	for y := 0; y < len(matrix); y++ {
		for x := 0; x < len(matrix[0]); x++ {
			if isAsterisk(coord(matrix, x, y)) {
				if numbers := adjacentNumbers(matrix, x, y); len(numbers) == 2 {
					gearSum += numbers[0] * numbers[1]
				}
			}
		}
	}

	return gearSum
}

func adjacentNumbers(matrix [][]byte, x, y int) []int {
	xMin := max(0, x-1)
	xMax := min(len(matrix[0])-1, x+1)

	yMin := max(0, y-1)
	yMax := min(len(matrix)-1, y+1)

	numbers := []int{}

	for y := yMin; y <= yMax; y++ {
		for x := xMin; x <= xMax; x++ {
			var number int

			number, x = numberAtPoint(matrix, x, y)
			if number > 0 {
				numbers = append(numbers, number)
			}
		}
	}

	return numbers
}

func numberAtPoint(matrix [][]byte, x, y int) (number int, newX int) {
	found := false

	for {
		if !isDigit(coord(matrix, x, y)) {
			break
		}
		found = true
		x--

		if x < 0 {
			break
		}
	}

	if !found {
		return 0, x
	}

	for {
		x++

		if x < len(matrix[0]) && isDigit(coord(matrix, x, y)) {
			number = number*10 + byteToInt(coord(matrix, x, y))
		} else {
			break
		}
	}

	return number, x - 1
}

func coord(matrix [][]byte, x, y int) byte {
	return matrix[y][x]
}

func isDigit(r byte) bool {
	return r >= '0' && r <= '9'
}

func isSymbol(r byte) bool {
	if isDigit(r) {
		return false
	}

	return r != '.'
}

func isAsterisk(r byte) bool {
	return r == '*'
}

func byteToInt(r byte) int {
	return int(r - '0')
}
