package main

import (
	"bytes"
	"fmt"
	"os"
)

func main() {
	data, err := os.ReadFile("source.txt")
	if err != nil {
		panic(err)
	}

	matrix := bytes.Split(bytes.TrimSpace(data), []byte("\n"))
	partsIDSum := 0

	for y := 0; y < len(matrix); y++ {
		for x := 0; x < len(matrix[0]); x++ {
			if isDigit(matrix[y][x]) {
				xMin := x - 1
				yMin := y - 1

				number := byteToInt(matrix[y][x])

				x++
				for {
					if x >= len(matrix[0]) {
						break
					}

					if isDigit(matrix[y][x]) {
						number = number*10 + byteToInt(matrix[y][x])
					} else {
						break
					}

					x++
				}
				x--

				xMax := x + 1
				yMax := y + 1

				if symbolInArea(matrix, xMin, yMin, xMax, yMax) {
					partsIDSum += number
				}
			}
		}
	}

	fmt.Println(partsIDSum)
}

func isDigit(r byte) bool {
	return r >= '0' && r <= '9'
}

func isSymbol(r byte) bool {
	if isDigit(r) {
		return false
	}

	return r != '.'
}

func byteToInt(r byte) int {
	return int(r - '0')
}

func symbolInArea(data [][]byte, xMin, yMin, xMax, yMax int) bool {
	for y := max(yMin, 0); y <= min(len(data)-1, yMax); y++ {
		for x := max(xMin, 0); x <= min(len(data[0])-1, xMax); x++ {
			if isSymbol(data[y][x]) {
				return true
			}
		}
	}
	return false
}
