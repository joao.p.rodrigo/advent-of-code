package main

import (
	"errors"
	"fmt"
	"os"
	"slices"
	"strconv"
	"strings"
)

func main() {
	data, err := os.ReadFile("source.txt")
	if err != nil {
		panic(err)
	}

	seeds, m := parseMap(data)

	var locs = make([]int, len(seeds))

	for i := range seeds {
		locs[i] = m.TranslateAll(seeds[i])
	}

	fmt.Println("lowest in seeds:", slices.Min(locs))

	rChan := make(chan int)
	routines := 0

	for i := 0; i < len(seeds); i += 2 {
		routines++

		go func(seedMin, seedMax int) {
			lowest := -1
			for seed := seedMin; seed < seedMax; seed++ {
				newLoc := m.TranslateAll(seed)
				if lowest == -1 || newLoc < lowest {
					lowest = newLoc
				}
			}

			rChan <- lowest

		}(seeds[i], seeds[i]+seeds[i+1])
	}

	lowest := -1
	for routines > 0 {
		n := <-rChan
		if lowest == -1 || n < lowest {
			lowest = n
		}

		routines--
	}

	fmt.Println("lowest in ranges:", lowest)
}

type SeedRanges []int

func (s SeedRanges) Execute(f func(int)) {
	for i := 0; i < len(s); i += 2 {
		for seed := s[i]; seed < s[i]+s[i+1]; seed++ {
			f(seed)
		}
	}
}

type FarmMap struct {
	ranges []MapRange
	next   *FarmMap
}

type MapRange struct {
	sourceIdx int
	destIdx   int
	len       int
}

func (m MapRange) Includes(idx int) bool {
	return idx >= m.sourceIdx && idx < m.sourceIdx+m.len
}

func (m MapRange) Translate(idx int) int {
	return idx - m.sourceIdx + m.destIdx
}

func (m FarmMap) Translate(idx int) int {
	for _, r := range m.ranges {
		if r.Includes(idx) {
			return r.Translate(idx)
		}
	}

	return idx
}

func (m FarmMap) TranslateAll(idx int) int {
	nextIdx := m.Translate(idx)
	if m.next == nil {
		return nextIdx
	}

	return m.next.TranslateAll(nextIdx)
}

func parseMap(m []byte) (seeds []int, fm FarmMap) {
	lines := strings.Split(string(m), "\n")
	seeds = parseSeeds(lines[0])

	root := FarmMap{}
	cur := &root

	for _, line := range lines[1:] {
		line = strings.TrimSpace(line)
		if len(line) == 0 {
			continue
		}

		if line[0] >= '0' && line[0] <= '9' {
			cur.ranges = append(cur.ranges, parseRange(line))

			continue
		}

		if len(cur.ranges) > 0 {
			cur.next = &FarmMap{}
			cur = cur.next
		}
	}

	return seeds, root
}

func parseSeeds(line string) []int {
	line = line[strings.IndexRune(line, ' ')+1:]

	nums := strings.Split(line, " ")
	r := make([]int, 0, len(nums))

	for _, s := range nums {
		n, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}

		r = append(r, n)
	}

	return r
}

func parseRange(line string) MapRange {
	nums := strings.Split(line, " ")
	m := MapRange{}

	n, err := strconv.Atoi(nums[0])
	m.destIdx = n

	n, e := strconv.Atoi(nums[1])
	m.sourceIdx = n
	err = errors.Join(err, e)

	n, e = strconv.Atoi(nums[2])
	m.len = n
	err = errors.Join(err, e)

	if err != nil {
		panic(err)
	}

	return m
}
