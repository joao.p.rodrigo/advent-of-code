package main

import (
	"bytes"
	"reflect"
	"slices"
	"testing"
)

const input = `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
`

func TestFarmMapTranslate(t *testing.T) {
	m := FarmMap{
		ranges: []MapRange{
			{
				sourceIdx: 98,
				destIdx:   50,
				len:       2,
			},
			{
				sourceIdx: 50,
				destIdx:   52,
				len:       48,
			},
		},
	}

	type mapCase struct {
		seed int
		soil int
	}

	cases := []mapCase{
		{0, 0},
		{1, 1},
		{48, 48},
		{49, 49},
		{50, 52},
		{96, 98},
		{97, 99},
		{98, 50},
		{99, 51},
	}

	for _, mc := range cases {
		if got := m.Translate(mc.seed); got != mc.soil {
			t.Errorf("expected %d, got %d", mc.soil, got)
		}
	}
}

func TestParseMap(t *testing.T) {
	seeds, m := parseMap(bytes.TrimSpace([]byte(input)))

	expectSeeds := []int{79, 14, 55, 13}
	if !reflect.DeepEqual(seeds, expectSeeds) {
		t.Errorf("seeds are not the same: expect: %v; got: %v", expectSeeds, seeds)
	}

	expectRange := MapRange{
		sourceIdx: 50,
		destIdx:   52,
		len:       48,
	}
	got := m.ranges[1]
	if !reflect.DeepEqual(got, expectRange) {
		t.Errorf("expected range %v, got %v", expectRange, got)
	}

	expectRange = MapRange{
		sourceIdx: 11,
		destIdx:   0,
		len:       42,
	}
	got = m.next.next.ranges[1]
	if !reflect.DeepEqual(got, expectRange) {
		t.Errorf("nested: expected range %v, got %v", expectRange, got)
	}
}

func TestLowest(t *testing.T) {
	seeds, m := parseMap(bytes.TrimSpace([]byte(input)))

	for i := range seeds {
		seeds[i] = m.TranslateAll(seeds[i])
	}

	lowest := slices.Min(seeds)

	if lowest != 35 {
		t.Errorf("expected %d; got %d", 35, lowest)
	}
}

func TestRangeLowest(t *testing.T) {
	seeds, m := parseMap(bytes.TrimSpace([]byte(input)))

	locs := make([]int, 0, 0)

	seedRange := SeedRanges(seeds)

	seedRange.Execute(func(seed int) {
		locs = append(locs, m.TranslateAll(seed))
	})

	lowest := slices.Min(locs)
	if lowest != 46 {
		t.Errorf("expected %d; got %d", 46, lowest)
	}
}
