package main

import (
	"fmt"
	"testing"
)

func TestHandWinnings(t *testing.T) {
	hands := parseHands([]byte(testInput))
	printHands(hands)
	sortHands(hands)
	printHands(hands)
	w := handWinnings(hands)

	expect := 5905
	if w != expect {
		t.Errorf("expect winnings %d, got %d", expect, w)
	}
}

func printHands(hands []hand) {
	for _, h := range hands {
		fmt.Printf("%v %s\n", string(h.cards), h.Combination())
	}
	fmt.Printf("\n")
}
