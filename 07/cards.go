package main

import (
	"bytes"
	"strconv"
)

var cards = []byte{'A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2', 'J'}

func cardIndex(card rune) int {
	return bytes.IndexRune(cards, card)
}

type Combination int

const (
	Unknown Combination = iota
	FiveOfAKind
	FourOfAKind
	FullHouse
	ThreeOfAKind
	TwoPair
	OnePair
	HighCard
)

func (c Combination) Compare(b Combination) int {
	if c > b {
		return 1
	} else if b > c {
		return -1
	}

	return 0
}

func (c Combination) String() string {
	return []string{
		"Unknown",
		"FiveOfAKind",
		"FourOfAKind",
		"FullHouse",
		"ThreeOfAKind",
		"TwoPair",
		"OnePair",
		"HighCard",
	}[int(c)]
}

type hand struct {
	cards       []rune
	combination Combination
	bid         int
}

func parseHands(data []byte) []hand {
	hands := make([]hand, 0)
	for _, line := range bytes.Split(data, []byte("\n")) {
		if len(line) == 0 {
			continue
		}
		hands = append(hands, parseHand(line))
	}

	return hands
}

func parseHand(line []byte) hand {
	parts := bytes.Split(line, []byte{' '})

	bid, _ := strconv.Atoi(string(parts[1]))

	return hand{
		cards: bytes.Runes(parts[0]),
		bid:   bid,
	}
}

func (h hand) Combination() Combination {
	if h.combination != Unknown {
		return h.combination
	}

	return calculateCombination(h)
}

func (h hand) Compare(b hand) int {
	aC := h.Combination()
	bC := b.Combination()

	cmp := aC.Compare(bC)

	if cmp != 0 {
		return cmp
	}

	for i := 0; i < len(h.cards); i++ {
		cmp = compareCards(h.cards[i], b.cards[i])

		if cmp == 0 {
			continue
		}

		return cmp
	}

	return 0
}

func compareCards(a, b rune) int {
	idxA := cardIndex(a)
	idxB := cardIndex(b)
	if idxA > idxB {
		return 1
	} else if idxB > idxA {
		return -1
	}

	return 0
}

func calculateCombination(h hand) Combination {
	maxEqual := 0

	counts := make([]int, len(cards))
	for _, r := range h.cards {
		idx := cardIndex(r)
		counts[idx]++
		if r != 'J' {
			maxEqual = max(maxEqual, counts[idx])
		}
	}

	jokerIdx := cardIndex('J')
	numJokers := counts[jokerIdx]
	counts[jokerIdx] = 0

	numberOfUniqueCards := 0
	for _, c := range counts {
		if c > 0 {
			numberOfUniqueCards++
		}
	}

	switch maxEqual {
	case 5:
		return FiveOfAKind
	case 4:
		if numJokers > 0 {
			return FiveOfAKind
		}
		return FourOfAKind
	case 3:
		switch numJokers {
		case 1:
			return FourOfAKind
		case 2:
			return FiveOfAKind
		}

		if numberOfUniqueCards == 2 {
			return FullHouse
		}

		return ThreeOfAKind

	case 2:
		switch numberOfUniqueCards {
		case 4:
			return OnePair
		case 3:
			return []Combination{TwoPair, ThreeOfAKind, FourOfAKind, FiveOfAKind}[numJokers]
		case 2:
			return []Combination{TwoPair, FullHouse, FourOfAKind, FiveOfAKind}[numJokers]
		case 1:
			return FiveOfAKind
		}

	case 1:
		return []Combination{HighCard, OnePair, ThreeOfAKind, FourOfAKind, FiveOfAKind}[numJokers]

	}

	return FiveOfAKind
}
