package main

import (
	"fmt"
	"os"
	"slices"
)

func main() {
	data, err := os.ReadFile("source.txt")
	if err != nil {
		panic(err)
	}

	hands := parseHands(data)

	sortHands(hands)
	fmt.Println(handWinnings(hands))
}

func sortHands(hands []hand) {
	slices.SortFunc(hands, func(a, b hand) int {
		return -a.Compare(b)
	})
}

func handWinnings(hands []hand) int {
	r := 0

	for i := 0; i < len(hands); i++ {
		rank := i + 1

		r += hands[i].bid * rank
	}

	return r
}
