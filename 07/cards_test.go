package main

import (
	"bytes"
	"reflect"
	"testing"
)

var testInput = `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483`

func TestParseHand(t *testing.T) {
	got := parseHand(bytes.Split([]byte(testInput), []byte("\n"))[0])
	expect := hand{
		cards: bytes.Runes([]byte("32T3K")),
		bid:   765,
	}

	if !reflect.DeepEqual(got, expect) {
		t.Errorf("expected %+v, got %+v", expect, got)
	}
}

func TestCalculateHandCombination(t *testing.T) {
	expect := []Combination{
		OnePair, FourOfAKind, TwoPair, FourOfAKind, FourOfAKind,
	}

	for i, hand := range parseHands([]byte(testInput)) {
		if got := hand.Combination(); got != expect[i] {
			t.Errorf("%s: expect %v, got %v", string(hand.cards), expect[i], got)
		}
	}

	type caseDef struct {
		hand   string
		expect Combination
	}
}
