package main

import (
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type LeaderBoard struct {
	RaceTime  int
	HighScore int
}

func main() {
	data, err := os.ReadFile("source.txt")
	if err != nil {
		panic(err)
	}

	boards := parseLeaderBoards(data)

	ways := 1
	for _, b := range boards {
		bw := b.PressBounds()
		fmt.Printf("%+v, ways: %d\n", b, bw)

		ways *= bw
	}

	fmt.Println("multiply: ", ways)

	fmt.Println("done")
}

var numParser = regexp.MustCompile(`\d+`)

func parseLeaderBoards(data []byte) []LeaderBoard {
	lines := strings.Split(string(data), "\n")
	lines[0] = strings.ReplaceAll(lines[0], " ", "")
	lines[1] = strings.ReplaceAll(lines[1], " ", "")

	strData := strings.Join(lines, "\n")
	numbersS := numParser.FindAllString(strData, -1)

	boards := []LeaderBoard{}

	for i := 0; i < len(numbersS)/2; i++ {
		raceTime, _ := strconv.Atoi(numbersS[i])
		highScore, _ := strconv.Atoi(numbersS[len(numbersS)/2+i])

		boards = append(boards, LeaderBoard{
			RaceTime:  raceTime,
			HighScore: highScore,
		})
	}

	return boards
}

func (l LeaderBoard) PressBounds() int {
	time := l.RaceTime
	distance := l.HighScore

	a := float64(-1)
	b := float64(time)
	c := float64(-distance)

	x1 := (-b + math.Sqrt(b*b-4*a*c)) / (2 * a)
	x2 := (-b - math.Sqrt(b*b-4*a*c)) / (2 * a)

	pressMin := math.Ceil(min(x1, x2))
	pressMax := math.Floor(max(x1, x2))

	if pressMin == min(x1, x2) {
		pressMin++
	}

	if pressMax == max(x1, x2) {
		pressMax--
	}

	return int(pressMax - pressMin + 1)
}
