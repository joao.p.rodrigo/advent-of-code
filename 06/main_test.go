package main

import "testing"

var TestRaces = []LeaderBoard{
	{RaceTime: 7, HighScore: 9},
	{RaceTime: 15, HighScore: 40},
	{RaceTime: 30, HighScore: 200},
}

func TestPressBounds(t *testing.T) {
	expectBounds := []int{
		4,
		8,
		9,
	}

	for i, expect := range expectBounds {
		if got := TestRaces[i].PressBounds(); got != expect {
			t.Errorf("race %+v: expected %d, got %d", TestRaces[i], expect, got)
		}
	}
}
