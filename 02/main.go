package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Game struct {
	ID          int
	Plays       []Play
	HighestPlay Play
}

type Play struct {
	Red   int
	Green int
	Blue  int
}

func (p Play) Higher(other Play) bool {
	return p.Red > other.Red || p.Blue > other.Blue || p.Green > other.Green
}

var AllowedCubesPlay = Play{
	Red:   12,
	Green: 13,
	Blue:  14,
}

func main() {
	f, err := os.Open("source.txt")
	if err != nil {
		panic(err)
	}

	count := 0

	pows := 0

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		game := parseGame(scanner.Text())

		if !game.HighestPlay.Higher(AllowedCubesPlay) {
			count += game.ID
		}

		pows += game.HighestPlay.Red * game.HighestPlay.Blue * game.HighestPlay.Green
	}

	fmt.Println("A: ", count)
	fmt.Println("B: ", pows)
}

func parseGame(s string) Game {
	g := Game{}

	idStr := s[strings.IndexRune(s, ' ')+1 : strings.IndexRune(s, ':')]
	id, _ := strconv.Atoi(idStr)
	g.ID = id

	gamesStr := s[strings.IndexRune(s, ':')+1:]

	for _, play := range strings.Split(gamesStr, ";") {
		colorsStr := strings.Split(play, ",")

		p := Play{}

		for _, colorStr := range colorsStr {
			colorStr = strings.TrimSpace(colorStr)

			valAndColor := strings.Split(colorStr, " ")
			val, _ := strconv.Atoi(valAndColor[0])

			switch valAndColor[1] {
			case "red":
				p.Red = val
				g.HighestPlay.Red = max(g.HighestPlay.Red, val)
			case "green":
				p.Green = val
				g.HighestPlay.Green = max(g.HighestPlay.Green, val)
			case "blue":
				p.Blue = val
				g.HighestPlay.Blue = max(g.HighestPlay.Blue, val)
			}
		}

		g.Plays = append(g.Plays, p)
	}

	return g
}
