package main

import (
	"bytes"
	"reflect"
	"strings"
	"testing"
)

const input = `
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
`

var unparsedCards = bytes.Split([]byte(strings.TrimSpace(input)), []byte("\n"))

var expectCards = []Card{
	{ID: 1, winningNumbers: []int{41, 48, 83, 86, 17}, cardNumbers: []int{83, 86, 6, 31, 17, 9, 48, 53}},
	{ID: 2, winningNumbers: []int{13, 32, 20, 16, 61}, cardNumbers: []int{61, 30, 68, 82, 17, 32, 24, 19}},
	{ID: 3, winningNumbers: []int{1, 21, 53, 59, 44}, cardNumbers: []int{69, 82, 63, 72, 16, 21, 14, 1}},
	{ID: 4, winningNumbers: []int{41, 92, 73, 84, 69}, cardNumbers: []int{59, 84, 76, 51, 58, 5, 54, 83}},
	{ID: 5, winningNumbers: []int{87, 83, 26, 28, 32}, cardNumbers: []int{88, 30, 70, 12, 93, 22, 82, 36}},
	{ID: 6, winningNumbers: []int{31, 18, 13, 56, 72}, cardNumbers: []int{74, 77, 10, 23, 35, 67, 36, 11}},
}

func TestParseCard(t *testing.T) {
	for i, card := range unparsedCards {
		if !reflect.DeepEqual(expectCards[i], parseCard(card)) {
			t.Fatalf("expected: %v; got: %v", expectCards[i], parseCard(card))
		}
	}
}

func TestPoints(t *testing.T) {
	if p := expectCards[0].Points(); p != 8 {
		t.Errorf("expected %d points, got %d", 8, p)
	}
}

func TestEarnedScratchCards(t *testing.T) {
	if got := earnedScratchCards(expectCards); got != 30 {
		t.Errorf("expected %d cards, got %d", 30, got)
	}
}
