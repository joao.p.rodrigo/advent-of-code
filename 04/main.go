package main

import (
	"bufio"
	"fmt"
	"os"
	"slices"
)

func main() {
	f, err := os.Open("source.txt")
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(f)

	v := 0
	cards := []Card{}
	for scanner.Scan() {
		card := parseCard(scanner.Bytes())
		v += card.Points()

		cards = append(cards, card)
	}

	fmt.Println(v)
	fmt.Println(earnedScratchCards(cards))
}

type Card struct {
	ID             int
	winningNumbers []int
	cardNumbers    []int
}

func (c Card) Points() int {
	matches := c.Matches()
	if matches == 0 {
		return 0
	}

	return 1 << (matches - 1)
}

func (c Card) Matches() int {
	p := 0
	for _, number := range c.cardNumbers {
		if slices.Contains(c.winningNumbers, number) {
			p++
		}
	}

	return p
}

type CardParsePhase int

const (
	PhaseID = iota
	PhaseWinning
	PhaseNumbers
)

func parseCard(line []byte) Card {
	tempNumber := 0
	currentPhase := PhaseID

	card := Card{}

	for i := 0; i <= len(line); i++ {
		var char byte
		if i < len(line) {
			char = line[i]
		}

		if char >= '0' && char <= '9' {
			tempNumber = tempNumber*10 + int(char-'0')
			continue
		}

		if tempNumber > 0 {
			switch currentPhase {
			case PhaseID:
				card.ID = tempNumber
				currentPhase++
			case PhaseWinning:
				card.winningNumbers = append(card.winningNumbers, tempNumber)
			case PhaseNumbers:
				card.cardNumbers = append(card.cardNumbers, tempNumber)
			}

			tempNumber = 0
		}

		if char == '|' {
			currentPhase++
		}
	}

	return card
}

func earnedScratchCards(cards []Card) int {
	wins := make([]int, len(cards), len(cards))

	for i := len(cards) - 1; i >= 0; i-- {
		numMatches := cards[i].Matches()

		numWins := 1
		for m := 1; m <= numMatches; m++ {
			numWins += wins[i+m]
		}
		wins[i] = numWins
	}

	cardStack := 0
	for _, w := range wins {
		cardStack += w
	}

	return cardStack
}
