package main

import (
	"fmt"
	"os"
)

type NodeStatus struct {
	n             *Node
	hops          int
	nextDirection int
}

func main() {
	data, err := os.ReadFile("source.txt")
	if err != nil {
		panic(err)
	}

	directions, data := parseDirections(data)
	nodes := parseNodes(data)
	nodes = StartNodes(nodes)

	zPositions := make([]int, 0, len(nodes))
	for _, node := range nodes {
		zPositions = append(zPositions, TraverseNodes(node, directions))
	}

	fmt.Println(LCM(zPositions[0], zPositions[1], zPositions[2:]...))
}

// LCM formula borrowed from https://go.dev/play/p/SmzvkDjYlb

// greatest common divisor (GCD) via Euclidean algorithm
func GCD(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

// find Least Common Multiple (LCM) via GCD
func LCM(a, b int, integers ...int) int {
	result := a * b / GCD(a, b)

	for i := 0; i < len(integers); i++ {
		result = LCM(result, integers[i])
	}

	return result
}
