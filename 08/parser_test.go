package main

import "testing"

const testInput = `LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)`

func TestTraverseNodes(t *testing.T) {
	directions, data := parseDirections([]byte(testInput))

	rootNode := parseNodes(data)

	expect := 6
	count := TraverseNodes(rootNode, directions)

	if count != expect {
		t.Errorf("expect hops %d, got %d", expect, count)
	}
}
