package main

import (
	"bytes"
	"fmt"
	"regexp"
	"strings"
)

type Node struct {
	ID        string
	Left      string
	Right     string
	endNode   bool
	startNode bool
	nextLeft  *Node
	nextRight *Node
}

var re = regexp.MustCompile(`\w\w\w`)

const hopLimit = 1_000_000_000_000

type Direction byte

const (
	DirectionLeft  Direction = 'L'
	DirectionRight Direction = 'R'
)

func (n *Node) Next(direction Direction) *Node {
	switch Direction(direction) {
	case DirectionRight:
		return n.nextRight
	case DirectionLeft:
		return n.nextLeft
	}

	panic(fmt.Sprintf("unknown direction: %v", direction))
}

func TraverseNodes(root *Node, directions []Direction) (hops int) {
	curNode := root
	for {
		if hops > hopLimit {
			panic("too many hops")
		}
		for i := 0; i < len(directions); i++ {
			hops++

			curNode = curNode.Next(directions[i])

			if curNode.endNode {
				return hops
			}
		}
	}
}

func StartNodes(nodes []*Node) []*Node {
	startNodes := []*Node{}

	for _, n := range nodes {
		if n.startNode {
			startNodes = append(startNodes, n)
		}
	}

	return startNodes
}

func parseDirections(data []byte) ([]Direction, []byte) {
	lineEnd := bytes.Index(data, []byte("\n\n"))
	line := data[:lineEnd]

	line = bytes.TrimSpace(line)

	directions := make([]Direction, 0, len(line))
	for _, d := range line {
		directions = append(directions, Direction(d))
	}
	return directions, data[lineEnd:]
}

func parseNodes(data []byte) []*Node {
	lines := strings.Split(string(data), "\n")

	nodes := make(map[string]*Node)

	for _, line := range lines {
		if len(line) == 0 {
			continue
		}

		coords := re.FindAllString(line, -1)

		node := Node{
			ID:        coords[0],
			Left:      coords[1],
			Right:     coords[2],
			startNode: coords[0][2] == 'A',
			endNode:   coords[0][2] == 'Z',
		}

		nodes[node.ID] = &node
	}

	nodeList := make([]*Node, 0, len(nodes))
	for _, node := range nodes {
		node.nextLeft = nodes[node.Left]
		node.nextRight = nodes[node.Right]

		nodeList = append(nodeList, node)
	}

	return nodeList
}
