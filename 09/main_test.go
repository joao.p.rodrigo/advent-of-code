package main

import (
	"strings"
	"testing"
)

const testInput = `0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45`

func TestExtrapolateNextValue(t *testing.T) {
	expectValues := []int{18, 28, 68}

	input := strings.Split(testInput, "\n")
	for i := range expectValues {
		got := extrapolateNextValue(parseValues(input[i]))
		expect := expectValues[i]

		if got != expect {
			t.Errorf("expect %d, got %d", expect, got)
		}
	}
}

func TestSum(t *testing.T) {
	gotSum := 0

	input := strings.Split(testInput, "\n")
	for _, line := range input {
		gotSum += extrapolateNextValue(parseValues(line))
	}

	expect := 114
	if gotSum != expect {
		t.Errorf("expect %d, got %d", expect, gotSum)
	}
}
