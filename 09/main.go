package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	data, err := os.ReadFile("source.txt")
	if err != nil {
		panic(err)
	}

	sumNext := 0
	sumPrev := 0

	for _, line := range strings.Split(string(data), "\n") {
		if len(line) == 0 {
			continue
		}

		values := parseValues(line)
		sumNext += extrapolateNextValue(values)
		sumPrev += extrapolatePreviousValue(values)

	}

	fmt.Println("next ", sumNext)
	fmt.Println("previous ", sumPrev)
}

func parseValues(line string) []int {
	s := strings.Split(line, " ")
	r := make([]int, len(s))

	for i := range s {
		v, err := strconv.Atoi(s[i])
		if err != nil {
			panic(err)
		}

		r[i] = v
	}

	return r
}

func extrapolateNextValue(input []int) int {
	if isZeroes(input) {
		return 0
	}

	return input[len(input)-1] + extrapolateNextValue(sequenceOfDifferences(input))
}

func extrapolatePreviousValue(input []int) int {
	if isZeroes(input) {
		return 0
	}

	return input[0] - extrapolatePreviousValue(sequenceOfDifferences(input))
}

func sequenceOfDifferences(input []int) []int {
	diffs := make([]int, len(input)-1)

	for i := 0; i < len(input)-1; i++ {
		diffs[i] = input[i+1] - input[i]
	}

	return diffs
}

func isZeroes(input []int) bool {
	for _, v := range input {
		if v != 0 {
			return false
		}
	}

	return true
}
